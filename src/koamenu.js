/*
 * Koamenu
 *
 *
 * Copyright (c) 2012 Kristijan Husak & Aleksandar Gosevski
 * Updated by: Ivan Tatic ( ivan@simplified.me )
 * Licensed under the MIT, GPL licenses.
 */

(function($) {
    var namespace = 'slidemenu';

    function SlideMenu(element, options) {
        var o = this.options = $.extend({}, $.fn.slideMenu.defaults, options);

        this.$element = $(element);
        this.$overlay = $('<div>', { 'class': 'overlay' });
        this.$menu = $(o.menu).hide();
        this.$content = $(o.mainContent).css({ position: 'relative', minHeight : window.innerHeight});
        this.isOpen = false;
        this.$content.append( this.$overlay.css( o.overlay ).hide() );
        this.$menu.css(o.direction, 0).css('width', o.menuWidth);
        this.$element.on('click.' + namespace, $.proxy(this.toggle, this));
        this.$overlay.on('click.' + namespace, $.proxy(this.toggle, this));
        window.addEventListener('orientationchange', $.proxy(this.resize, this));
    }

    SlideMenu.prototype = {
        $: function (selector) {
            return this.$element.find(selector);
        },
        toggle: function(e) {
            e && e.preventDefault();
            this._toggleState();
        },
        _toggleState: function() {
            var o = this.options,
                direction = (o.direction === 'left') ? o.menuWidth : '-' + o.menuWidth,
                animation = this.isOpen ? '0%' : direction,
                speed = o.speed/1000 + 's',
                self = this,
                contentCss = {
                    'min-height' : this.isOpen ? window.innerHeight : this.$menu.height(),
                    'left' : animation
                };

                $.extend(contentCss, this.transition('all', speed));

            this.$content.css(contentCss);

            this.$overlay.fadeToggle(o.speed);

            this.isOpen = !this.isOpen;

            if (this.isOpen)
                this.$menu.fadeIn(o.speed);
            else
                this.$menu.fadeOut(o.speed);


        },
        resize: function () {
            this.$content.css('min-height', window.innerHeight);
        },

        transition: function (prop, speed) {
            return {
                WebkitTransition : prop + ' ' + speed,
                MozTransition    : prop + ' ' + speed,
                MsTransition     : prop + ' ' + speed,
                OTransition      : prop + ' ' + speed,
                transition       : prop + ' ' + speed
            };
        }
    };

    $.fn.slideMenu = function (option) {
        return this.each(function () {
            var $this = $(this),
            data = $this.data(namespace),
            options = typeof option === 'object' && option;

            if ( !data ) {
                $this.data(namespace, (data = new SlideMenu(this, options)));
            }

            if ( typeof option === 'string' && option.charAt(0) !== '_') {
                data[option]();
            }
        });
    };

    $.fn.slideMenu.Constructor = SlideMenu;
    $.fn.slideMenu.defaults = {
        direction : 'left',
        menu : '.menu',
        mainContent : '.main-content',
        speed : 300,
        menuWidth : '50%',
        overlay: {
            left: 0,
            right: 0,
            bottom: 0,
            top: 0,
            position: 'absolute',
            background: 'rgba(0,0,0,.4)',
            userSelect: 'none',
            zIndex: '99'
        }
    };

}(jQuery));

module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({

        concat: {
            dist: {
                src: ['src/koamenu.js'],
                dest: 'dist/koamenu.js'
            }
        },

        uglify: {
            project_production: {
                files: {
                    'dist/koamenu.min.js': ['dist/koamenu.js']
                }
            }
        },

        watch: {
            files: ['src/koamenu.js'],
            tasks: ['concat', 'uglify']
        }

    });

    // Default task.
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.registerTask('default', ['concat', 'uglify']);

};
